<?php

declare(strict_types=1);

namespace App\Tests\Catalog\Value;

use App\Catalog\Value\Amount;
use App\Catalog\Exception\AmountBelowZeroException;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\equalTo;

/** @covers \App\Catalog\Value\Amount */
final class AmountTest extends TestCase {
    
    private $amount;

    protected function setUp(): void {
        $this->amount = new Amount(1000);
    }

    /** @test */
    public function getCents_WithValidCents_ReturnsUnchangedCents(): void {
        $cents = $this->amount->getCents();
        self::assertEquals(1000, $cents);
    }
    
    /** @test */
    public function getCents_WithValidCents_ReturnsUnchangedCents_WithAssertThat(): void {
        $cents = $this->amount->getCents();
        self::assertThat($cents, self::equalTo(1000));
    }
    /** @test */
    public function constructor_WithNegativeCents_ThrowsException(): void {
        $this->expectException(AmountBelowZeroException::class);
        new Amount(-1);
    }
}