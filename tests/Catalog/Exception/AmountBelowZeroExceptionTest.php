<?php

declare(strict_types=1);

namespace App\Tests\Catalog\Exception;

use App\Catalog\Exception\AmountBelowZeroException;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\equalTo;

/** @covers \App\Catalog\Exception\AmountBelowZeroException */
final class AmountBelowZeroExceptionTest extends TestCase {

    /** @test */
    public function fromInt_WithCents_SetsMessageContainingCents(): void {
        $exception = AmountBelowZeroException::fromInt(-10);

        $expected ='Money amount cannot be below zero. -10 given';
        $actual = $exception->getMessage();
        $errorMessage = 
            "Incorrect exception message received";

        self::assertThat($expected, equalTo($actual), $errorMessage);
    }
}