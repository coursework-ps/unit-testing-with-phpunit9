<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Handler;

use App\Catalog\Handler\ProductListHandler;
use App\Catalog\Repository\ProductRepository;
use App\Catalog\SearchAnalytics\SearchAnalytics;
use Psr\Http\Message\ServerRequestInterface;
use App\Catalog\Value\Amount;
use App\Catalog\Value\Product;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertNotEmpty;
use function PHPUnit\Framework\assertThat;
use function PHPUnit\Framework\equalTo;

/** @covers \App\Catalog\Handler\ProductListHandler */
final class ProductListHandlerTest extends TestCase {

    private $searchAnalytics;
    private $repository;
    private $serverRequestInterface;

    /** @before  */
    public function setUp(): void {
        parent::setUp();
        $this->searchAnalytics = $this->createMock(SearchAnalytics::class);
        $this->repository = $this->createMock(ProductRepository::class);
        $this->serverRequestInterface = $this->createMock(ServerRequestInterface::class);
    }

    /** @test */
    public function handle_withValidEventData_returnsEventData(): void {

        $this->repository->method('findProducts')->willReturn([
            new Product('Concert 1', new Amount(1100)),
            new Product('Concert 2', new Amount(550))
        ]);

        $this->searchAnalytics
            ->expects($this->once())
            ->method('track')
            ->with(['price' => null, 'name' => null]);

        $handler = new ProductListHandler($this->repository, $this->searchAnalytics);
        $response = $handler->handle($this->serverRequestInterface);

        $actualEvents = array();

        // Intelliphense plugin can't find getPayload function, but it's there. Trust me.
        foreach ($response->getPayload() as $event) {
            $actualEvents[$event['name']] = $event['price'];
        }

        assertNotEmpty($actualEvents);
        assertThat($actualEvents['Concert 1'], equalTo("11.00"));
        assertThat($actualEvents['Concert 2'], equalTo("5.50"));
    }
}