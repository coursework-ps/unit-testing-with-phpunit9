# PHPUnit Demo

This project is a modification of a project created for the Pluralsight course "Unit Testing with PHPUnit 9". The project demonstrates the use of the PHPUnit testing framework. The tests may be run locally within an integrated development environment (IDE), or they may be run during the execution of a GitLab CI pipeline.

## Local Testing

Blah

## GitLab CI Pipeline Testing

Blah
