# Launch all services (containers), open a bash shell, and
# execute a command that installs Composer. Run this command
# immediately after cloning the project, before you run the
# 'test' command for the first time. It should need to be
# run only once after cloning the project.
init:
	docker compose up -d --build && docker compose run php bash -c "composer install"

# Launch the php and mysql services (containers)
start:
	docker compose up -d

# Open a bash shell, and execute a command that runs the tests using PHPUnit
test:
	docker compose run php bash -c "vendor/bin/phpunit -c phpunit.xml --coverage-text --colors=never"

# Shut down and remove all running Docker containers
stop:
	docker compose down