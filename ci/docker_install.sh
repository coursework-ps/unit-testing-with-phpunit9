#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Get the latest software packages
apt-get update -yqq

# Install zip and unzip
apt-get install zip unzip

# Install wget
apt-get install wget -yqq

# Install git (the php image doesn't have it) which is required by composer
apt-get install git -yqq

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit.phar"
chmod +x /usr/local/bin/phpunit

# Install mysql driver
docker-php-ext-install pdo_mysql

# Install MySQL Client
apt-get install -y default-mysql-client